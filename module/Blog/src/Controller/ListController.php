<?php 
namespace Blog\Controller;

use Blog\Model\PostRepositoryInterface;
use Laminas\Mvc\Controller\AbstractActionController;

use Laminas\View\Model\ViewModel;
use InvalidArgumentException;

class ListController extends AbstractActionController
{
	private $postRepository;

	public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function indexAction()
    {
        return new ViewModel([
            'posts' => $this->postRepository->findAllPosts(),
        ]);
    }

    public function detailAction()
    {
	if (true){
		echo "This is test code";
	}
	if ('a' == 'a' and 'b' == 'b')
	{
		echo "This is second code";
	}
    	$id = $this->params()->fromRoute('id');
    	try {
	        $post = $this->postRepository->findPost($id);
	    } catch (\InvalidArgumentException $ex) {
	        return $this->redirect()->toRoute('blog');
	    }

	    return new ViewModel([
	        'post' => $post,
	    ]);
    }

}